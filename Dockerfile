FROM debian:9 as deb

RUN apt update && apt install -y gcc wget unzip make libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev

RUN mkdir -p tmp/nginx-build
RUN mkdir -p /usr/local/luajit
WORKDIR /tmp/nginx-build
RUN wget https://github.com/openresty/luajit2/archive/refs/tags/v2.1-20220411.tar.gz
RUN tar xzvf v2.1-20220411.tar.gz && cd luajit2-2.1-20220411 && make PREFIX=/usr/local/luajit \
&& make install PREFIX=/usr/local/luajit

RUN wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.1.tar.gz \
&& wget https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.21rc2.tar.gz\
&& tar xzvf v0.3.1.tar.gz && tar xzvf v0.10.21rc2.tar.gz

RUN wget https://nginx.org/download/nginx-1.19.3.tar.gz && tar xzvf nginx-1.19.3.tar.gz \
&& cd nginx-1.19.3 \
&& export LUAJIT_LIB=/usr/local/luajit/lib \
&& export LUAJIT_INC=/usr/local/luajit/include/luajit-2.1 \
&& ./configure --prefix=/usr/local/nginx \
         --with-ld-opt="-Wl,-rpath,/usr/local/luajit/lib" \
         --add-module=/tmp/nginx-build/ngx_devel_kit-0.3.1 \
         --add-module=/tmp/nginx-build/lua-nginx-module-0.10.21rc2 \
&& make -j2 && make install

RUN wget https://github.com/openresty/lua-resty-core/archive/refs/heads/master.zip -O lua-resty-core.zip\
&& unzip lua-resty-core.zip && cd lua-resty-core-master \
&& make install PREFIX=/usr/local/nginx

RUN wget -c https://github.com/openresty/lua-resty-lrucache/archive/refs/heads/master.zip \
&& unzip master.zip && cd lua-resty-lrucache-master \
&&  make install PREFIX=/usr/local/nginx

RUN echo "<html><br><h1> NGINX WITH LUA WORK! </h1>" > /usr/local/nginx/html/index.html
#COPY nginx.conf /usr/local/nginx/conf/nginx.conf

from debian:9
WORKDIR /usr/local/nginx/
COPY --from=deb /usr/local/nginx/ .
COPY --from=deb /usr/local/luajit /usr/local/luajit
COPY --from=deb /usr/local/share /usr/local/share

EXPOSE 80

CMD ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]
